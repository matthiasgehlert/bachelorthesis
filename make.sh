#!/bin/sh
set -o errexit -o noclobber -o nounset
if [ "${1:-}" = "app" ]; then
	type R > /dev/null 2>&1 || \
		{ printf "\\terror: R(1) unavailable.\\n" >&2; exit 1; }
	[ -f shinyapplication.r ] || \
		{ printf "\\terror: shinyapplication.r unavailable.\\n" >&2; exit 1; }
	_host=127.0.0.1
	_port=8880
	Rscript -e "shiny::runApp(appDir = \"shinyapplication.r\", host = \"$_host\", port = $_port)"
	unset _host _port
elif [ "${1:-}" = "thesis" ]; then
	type latexmk > /dev/null 2>&1 || \
		{ printf "\\terror: latexmk(1) unavailable.\\n" >&2; exit 1; }
	[ -f latexthesis.tex ] || \
		{ printf "\\terror: latexthesis.tex unavailable.\\n" >&2; exit 1; }
	latexmk -xelatex -Werror latexthesis.tex
else
	printf "\\terror: no valid target, see README.\\n" >&2
	exit 1
fi
