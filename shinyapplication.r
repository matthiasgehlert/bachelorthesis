# Aggregating of conditional effects in logistic regression
# Copyright (c) 2019-20 Matthias Gehlert <matthiasgehlert [at] posteo.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 2.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set.seed(647)
pkgs <- c("shiny", "ggplot2")
lapply(pkgs, require, character.only = TRUE); rm(pkgs)

cat("\nCopyright (c) 2019-20 Matthias Gehlert <matthiasgehlert [at] posteo.net>
This program comes with ABSOLUTELY NO WARRANTY; for details see COPYING.
This is free software, and you are welcome to redistribute it
under certain conditions; see COPYING for details.\n")

VERBOSE <- FALSE
BUILD <- paste0("VERSION 0.3-b", format(Sys.Date(), format = "%Y%m%d"))
COLORSCHEME <- c("#ffffff", "#d90368", "#00aacf", "#083d77", "#13293d")

rnd <- function() {
	a <- do.call(paste0, replicate(5, sample(letters, 1, TRUE), FALSE))
	b <- sprintf("%04d", sample(9999, 1, TRUE))
	c <- sample(letters, 1, TRUE)
	paste0(a, b, c)
}

genrow1 <- function(x, n1 = 12) { fluidRow(column(width = n1, x)) }
genrow2 <- function(x, y, n1 = 6, n2 = 6) { fluidRow(column(width = n1, x), column(width = n2, y)) }

css <- "
	* {
		color: #606060;
		font-family: monospace;
		font-size: 11pt;
	}
	body {
		background-color: #ffffff;
		padding-bottom: 15px;
		padding-top: 15px;
	}
	hr {
		font-weight: 800;
	}

	.title {
		color: #083d77;
		font-size: 1.5em;
		font-weight: 900;
		padding-bottom: 5px;
		padding-top: 10px;
	}
	.title > .MathJax,
	.title > .MathJax_Preview {
		color: #083d77;
	}
	.subtitle {
		color: #083d77;
		padding-bottom: 5px;
	}
	.subsubtitle {
		color: #083d77;
		font-weight: 900;
	}

	.adjustment-1 {
		padding-right: 10px;
	}
	.adjustment-2 {
		padding-left: 10px;
	}
	.adjustment-3 {
		padding-top: 10px;
	}

	.MathJax_Preview {
		visibility: hidden;
	}
	.MathJax,
	.MathJax_Display {
		font-size: 60%;
	}
	.MathJax_Display {
		padding-bottom: 5px;
		padding-top: 5px;
	}

	.col-sm-4 > .well {
		background-color: #ffffff;
	}
	.help-block {
		color: #606060;
		text-align: justify;
		word-wrap: break-word;
	}
	.irs-bar,
	.irs-bar-edge,
	.irs-from,
	.irs-max,
	.irs-min,
	.irs-single,
	.irs-to {
		background-color: #083d77;
		border-color: #083d77;
		color: #ffffff;
	}
	.irs-line {
		background: none;
	}
	.irs-slider {
		background: #ffffff;
	}
	.nav-pills > li > a {
		padding: 5px 10px;
	}
	.nav-pills > li.active > a,
	.nav-pills > li.active > a:focus,
	.nav-pills > li.active > a:hover {
		background-color: #083d77;
	}
	.nav.nav-pills.shiny-tab-input.shiny-bound-input {
		background-color: #ffffff;
		border-radius: 4px;
		border: 1px solid #e3e3e3;
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
		margin-bottom: 30px;
		min-height: 10px;
		padding: 10px;
	}
	.recalculating {
		opacity: 0;
	}
	.selectize-input.full {
		background-color: #ffffff;
	}
	.selectize-input.focus {
		border-color: #083d77;
		box-shadow: none;
	}
	.shiny-output-error {
		visibility: hidden;
	}
	.shiny-text-output.shiny-bound-output {
		background-color: transparent;
		color: #083d77;
	}
	.tab-content {
		border-radius: 4px;
		border: 1px solid #e3e3e3;
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
		margin-top: 30px;
		padding: 15px 20px;
	}
"

input_distribution.id <- rnd()
input_example.id <- rnd()
input_help.id <- rnd()
input_method.id <- rnd()
input_n.id <- rnd()
input_pX1gZ_kappa0.id <- rnd()
input_pX1gZ_kappa1.id <- rnd()
input_pY1gXZ_lambda00.id <- rnd()
input_pY1gXZ_lambda01.id <- rnd()
input_pY1gXZ_lambda10.id <- rnd()
input_pY1gXZ_lambda11.id <- rnd()
input_rnorm_mu.id <- rnd()
input_rnorm_sd.id <- rnd()
input_submit.id <- rnd()
input_submit.id2 <- rnd()
output_ADJ.id <- rnd()
output_ADJ.id2 <- rnd()
output_download.id <- rnd()
output_download.id2 <- rnd()
output_MEM.id <- rnd()
output_MEM.id2 <- rnd()
output_plots.id <- rnd()
output_plots_pX1gZ.id <- rnd()
output_plots_pY1gXZ.id <- rnd()
output_preview.id <- rnd()
output_preview.id2 <- rnd()
output_SME.id <- rnd()
output_SME.id2 <- rnd()
ui_tabs.id <- rnd()

input_rnorm_mu.min <- -3
input_rnorm_sd.min <- 0.25
input_pX1gZ_kappa0.min <- -3
input_pX1gZ_kappa1.min <- -3
input_pY1gXZ_lambda00.min <- -3
input_pY1gXZ_lambda01.min <- -3
input_pY1gXZ_lambda10.min <- -3
input_pY1gXZ_lambda11.min <- -3
input_n.min <- 0

input_rnorm_mu.max <- 3
input_rnorm_sd.max <- 3
input_pX1gZ_kappa0.max <- 3
input_pX1gZ_kappa1.max <- 3
input_pY1gXZ_lambda00.max <- 3
input_pY1gXZ_lambda01.max <- 3
input_pY1gXZ_lambda10.max <- 3
input_pY1gXZ_lambda11.max <- 3
input_n.max <- 100000

input_rnorm_mu.step <- 0.25
input_rnorm_sd.step <- 0.25
input_pX1gZ_kappa0.step <- 0.25
input_pX1gZ_kappa1.step <- 0.25
input_pY1gXZ_lambda00.step <- 0.25
input_pY1gXZ_lambda01.step <- 0.25
input_pY1gXZ_lambda10.step <- 0.25
input_pY1gXZ_lambda11.step <- 0.25
input_n.step <- 2000

input_rnorm_mu.val1 <- 0
input_rnorm_sd.val1 <- 1
input_pX1gZ_kappa0.val1 <- 0
input_pX1gZ_kappa1.val1 <- 0
input_pY1gXZ_lambda00.val1 <- 0
input_pY1gXZ_lambda01.val1 <- 0
input_pY1gXZ_lambda10.val1 <- 0
input_pY1gXZ_lambda11.val1 <- 0

input_rnorm_mu.val2 <- 0
input_rnorm_sd.val2 <- 1
input_pX1gZ_kappa0.val2 <- 0
input_pX1gZ_kappa1.val2 <- 0
input_pY1gXZ_lambda00.val2 <- 2.5
input_pY1gXZ_lambda01.val2 <- 1
input_pY1gXZ_lambda10.val2 <- 0
input_pY1gXZ_lambda11.val2 <- 0

input_rnorm_mu.val3 <- input_rnorm_mu.val2
input_rnorm_sd.val3 <- input_rnorm_sd.val2
input_pX1gZ_kappa0.val3 <- input_pX1gZ_kappa0.val2
input_pX1gZ_kappa1.val3 <- 2
input_pY1gXZ_lambda00.val3 <- input_pY1gXZ_lambda00.val2
input_pY1gXZ_lambda01.val3 <- input_pY1gXZ_lambda01.val2
input_pY1gXZ_lambda10.val3 <- input_pY1gXZ_lambda10.val2
input_pY1gXZ_lambda11.val3 <- input_pY1gXZ_lambda11.val2

input_rnorm_mu.val4 <- input_rnorm_mu.val2
input_rnorm_sd.val4 <- input_rnorm_sd.val2
input_pX1gZ_kappa0.val4 <- input_pX1gZ_kappa0.val2
input_pX1gZ_kappa1.val4 <- 0
input_pY1gXZ_lambda00.val4 <- input_pY1gXZ_lambda00.val2
input_pY1gXZ_lambda01.val4 <- input_pY1gXZ_lambda01.val2
input_pY1gXZ_lambda10.val4 <- 2.5
input_pY1gXZ_lambda11.val4 <- 1

input_rnorm_mu.val5 <- input_rnorm_mu.val2
input_rnorm_sd.val5 <- input_rnorm_sd.val2
input_pX1gZ_kappa0.val5 <- input_pX1gZ_kappa0.val2
input_pX1gZ_kappa1.val5 <- 2
input_pY1gXZ_lambda00.val5 <- input_pY1gXZ_lambda00.val2
input_pY1gXZ_lambda01.val5 <- input_pY1gXZ_lambda01.val2
input_pY1gXZ_lambda10.val5 <- input_pY1gXZ_lambda10.val4
input_pY1gXZ_lambda11.val5 <- input_pY1gXZ_lambda11.val4

input_rnorm_mu.val6 <- -0.5
input_rnorm_sd.val6 <- 3
input_pX1gZ_kappa0.val6 <- 1
input_pX1gZ_kappa1.val6 <- 0
input_pY1gXZ_lambda00.val6 <- -3
input_pY1gXZ_lambda01.val6 <- 2.5
input_pY1gXZ_lambda10.val6 <- 1.25
input_pY1gXZ_lambda11.val6 <- -2.25

input_rnorm_mu.val7 <- input_rnorm_mu.val6
input_rnorm_sd.val7 <- input_rnorm_sd.val6
input_pX1gZ_kappa0.val7 <- input_pX1gZ_kappa0.val6
input_pX1gZ_kappa1.val7 <- 0.5
input_pY1gXZ_lambda00.val7 <- input_pY1gXZ_lambda00.val6
input_pY1gXZ_lambda01.val7 <- input_pY1gXZ_lambda01.val6
input_pY1gXZ_lambda10.val7 <- input_pY1gXZ_lambda10.val6
input_pY1gXZ_lambda11.val7 <- input_pY1gXZ_lambda11.val6

input_distribution.item1 <- "Normal distribution"
input_example.item1 <- "NO effect, X and Z are INdependent"
input_example.item2 <- "NO treatment effect, X and Z are INdependent"
input_example.item3 <- "NO treatment effect, X and Z are dependent"
input_example.item4 <- "Treatment effect, X and Z are INdependent"
input_example.item5 <- "Treatment effect, X and Z are dependent"
input_example.item6 <- "Treatment effect, X and Z are INdependent [Inversion of MEM]"
input_example.item7 <- "Treatment effect, X and Z are dependent [Inversion of SME and MEM]"
input_example.item8 <- "Randomly choosen parameters"
input_help.item1 <- "General information"
input_help.item2 <- "References"
input_method.item1 <- "Immediate update of computation (i.e., if input changed)"
input_method.item2 <- "Delayed update of computation (i.e., if button pressed)"
output_plots.item1 <- "(X=x,Z=z)-conditional probabilities of success"
output_plots.item2 <- "(Z=z)-conditional probabilities of treatment"

calculations_plots2_axis.lb <- "Z"
calculations_plots2_axis.lb2 <- "P(X=1|Z)"
calculations_plots2_legend.lb <- "P(X=1|Z)"
calculations_plots_axis.lb <- "Z"
calculations_plots_axis.lb2 <- "P(Y=1|X=x,Z)"
calculations_plots_legend.lb <- "P(Y=1|X=0,Z)"
calculations_plots_legend.lb2 <- "P(Y=1|X=1,Z)"
input_distribution.lb <- "Distribution"
input_example.lb <- "EXAMPLE"
input_help.lb <- "Topic"
input_method.lb <- "Update policy"
input_n.lb <- "\\(N\\)"
input_pX1gZ_kappa0.lb <- "\\(\\kappa_{0}\\)"
input_pX1gZ_kappa0.lb2 <- "kappa0"
input_pX1gZ_kappa1.lb <- "\\(\\kappa_{1}\\)"
input_pX1gZ_kappa1.lb2 <- "kappa1"
input_pY1gXZ_lambda00.lb <- "\\(\\lambda_{00}\\)"
input_pY1gXZ_lambda00.lb2 <- "lambda00"
input_pY1gXZ_lambda01.lb <- "\\(\\lambda_{01}\\)"
input_pY1gXZ_lambda01.lb2 <- "lambda01"
input_pY1gXZ_lambda10.lb <- "\\(\\lambda_{10}\\)"
input_pY1gXZ_lambda10.lb2 <- "lambda10"
input_pY1gXZ_lambda11.lb <- "\\(\\lambda_{11}\\)"
input_pY1gXZ_lambda11.lb2 <- "lambda11"
input_rnorm_mu.lb <- "\\(\\mu_{Z}\\)"
input_rnorm_mu.lb2 <- "mu_Z"
input_rnorm_sd.lb <- "\\(\\sigma_{Z}\\)"
input_rnorm_sd.lb2 <- "sd_Z"
input_submit.lb <- "COMPUTE aggregated effects <br/> and visualizations"
input_subsubtitle.lb <- "Pre-specified scenario"
input_subsubtitle.lb2 <- "User-specified scenario"
output_download.lb <- "Download data set"
output_plots.lb <- "Plots"
output_subsubtitle.lb <- "Visualizations of the conditional probabilities"
output_subsubtitle.lb2 <- "Aggregated effects"
ui_tabs.lb <- "MODEL SPECIFICATION"
ui_tabs.lb2 <- "GENERATE DATA"
ui_tabs.lb3 <- "ABOUT"
ui_title.lb <- "Aggregation\\(^{[0,1]}\\)Xplorer"
ui_title.lb2 <- "Aggregation[0,1]Xplorer"
ui_title.lb3 <- "aggregation_01_xplorer"
ui_title.lb4 <- "Aggregating of conditional effects in logistic regression"

input_distribution.d <- "Below you can specify the distribution of the covariate \\(Z\\)."
input_distribution.d2 <- "Below you can now specify the parameters for the distribution with"
input_example.d <- "Below you can select an example with specified input parameters to simulate a certain scenario."
input_help.d <- "Below you can select a topic. For specific usage information refer to the help above the respective input element."
input_method.d <- "Below you can select the update policy of the aggregated effects and visualizations."
input_n.d <- "Below you can select the sample size for the data set to be generated."
input_pX1gZ.d <- "Below you can specify the coefficients of the logistic regression \\[P(X{=}1|Z) = \\frac{\\exp(\\kappa_{0} + \\kappa_{1} \\cdot Z)}{1 + \\exp(\\kappa_{0} + \\kappa_{1} \\cdot Z)}\\]"
input_pY1gXZ.d <- "Below you can specify the coefficients of the logistic regression \\[P(Y{=}1|X,Z) = \\frac{\\exp(f_{0}(Z) + f_{1}(Z) \\cdot X)}{1 + \\exp(f_{0}(Z) + f_{1}(Z) \\cdot X)}\\] where"
input_pY1gXZ.d2 <- "\\[f_{0} = \\lambda_{00} + \\lambda_{01} \\cdot Z\\]"
input_pY1gXZ.d3 <- "\\[f_{1} = \\lambda_{10} + \\lambda_{11} \\cdot Z\\]"
input_submit.d <- "Below you can confirm the input and compute the aggregated effects and visualizations."
output_ADJ.d <- "The adjusted marginal effects (ADJ) are calculated based on the \\(Z\\)-adjusted conditional expectations \\(E\\left[P(Y{=}1|X{=}x,Z)\\right]\\). This is an aggregation on the probability scale. This principle of aggregation can be used to compute aggregated effects on the probability scale (e.g. \\(E\\left[g_{1}(Z)\\right]\\)), the odds ratio scale and the log odds ratio scale. The conditional probabilities are causally unbiased [1], irrespective of whether the independent variables are stochastically independent (i.e., if \\(X \\perp \\!\\!\\! \\perp Z\\)). \\[E\\left[g_{1}(Z)\\right] = E\\left[P(Y{=}1|X{=}1,Z)\\right] - E\\left[P(Y{=}1|X{=}0,Z)\\right]\\]\\[OR_{adj} = \\frac{E\\left[P(Y{=}1|X{=}1,Z)\\right]}{1 - E\\left[P(Y{=}1|X{=}1,Z)\\right]} \\bigg/ \\frac{E\\left[P(Y{=}1|X{=}0,Z)\\right]}{1 - E\\left[P(Y{=}1|X{=}0,Z)\\right]}\\]\\[logOR_{ADJ} = \\ln(OR_{ADJ})\\]"
output_ADJ.d2 <- "[1] A detailed explanation with theoretical background and further references can be found in the accompanying bachelor thesis (Gehlert, 2019)."
output_download.d <- "Below you can download the data set [1] as comma-separated .csv-file. It can be used to analyze the data with any external statistical program (STATA, SPSS, SAS, R, ..).<br/><br/>[1] The download <i>might</i> be corrupted with sample sizes above 30,000."
output_download.d2 <- "Below you can see a preview of the data set. It contains an neglectable identifier (ID), the binary treatment variable (X), the binary response variable (Y) and the covariate (Z)."
output_help_citation.d <- "R Core Team. (2019). R: A Language and Environment for Statistical Computing and Graphics. Retrieved from https://r-project.org"
output_help_citation.d2 <- "Chang, W., Cheng, J., Allaire, J., Xie, Y. & McPherson, J. (2018). shiny: Web Application Framework for R. Retrieved from https://cran.r-project.org/package=shiny"
output_help_citation.d3 <- "Wickham, H. & Chang, W. (2018). ggplot2: Create Elegant Data Visualisations Using the Grammar of Graphics. Retrieved from https://cran.r-project.org/package=ggplot2"
output_help_citation.d4 <- "Steyer, R. (2018; in preparation). Probability and Causality. Volume I. Causal Total Effects. Book in preparation."
output_help_citation.d5 <- "Steyer, R. & Nagel, W. (2017). Probability and Conditional Expectation: Fundamentals for the Empirical Sciences. Wiley Series in Probability and Statistics. Chichester: John Wiley & Sons Ltd. doi:10.1002/9781119243496IX"
output_help_citation.d6 <- "In terms of the theoretical background, mathematical notations and causal theory:"
output_help_citation.d7 <- "In terms of the technical implementation of this application:"
output_help_citation2.d <- "Acknowledgment. I would like to express my sincere gratitude to the department of methodology and evaluation research at the Friedrich-Schiller-University Jena [1] and especially to Prof. Dr. Rolf Steyer and Adrian Jusepeitis M.Sc. for their continuous support and patience."
output_help_citation2.d2 <- "[1] https://metheval.uni-jena.de"
output_help_general.d <- "The <b>\"Aggregation[0,1]Xplorer for binary response variable\"</b> is an interactive tool to explore the effect of a binary treatment variable on a binary response variable, while aggregating over the values of a covariate. There are three basic strategies (SME, MEM, ADJ) described in the literature. The simple marginal effects (SME) are calculated based on the conditional probability \\(P(Y{=}1|X)\\). In a strict sense, this is not an aggregation of the conditional probability, because \\(Z\\) is simply ignored. The marginal effects at the mean (MEM) are calculated based on the (\\(X,Z\\))-conditional probability \\(P(Y{=}1|X,Z)\\), where \\(Z{=}\\mu_{Z}\\). This is only an aggregation on the log odds scale. The adjusted marginal effects (ADJ) are calculated based on the \\(Z\\)-adjusted conditional expectations \\(E\\left[P(Y{=}1|X{=}x,Z)\\right]\\). This is an aggregation on the probability scale. All of these three principles can each be used to calculate (aggregated) effects on the probability scale, the odds ratio scale and the log odds ratio scale. However, only the conditional expectations \\(E\\left[P(Y{=}1|X{=}x,Z)\\right]\\) and the corresponding aggregated effects are causally unbiased, irrespective of whether the independent variables are stochastically independent (i.e., if \\(X \\perp \\!\\!\\! \\perp Z\\))."
output_help_general.d2 <- "You can find more information about this shiny application (theoretical background, usage and license) in the accompanying bachelor thesis and the public repository:"
output_help_general.d3 <- "Gehlert, M. (2019). Zur Aggregation von bedingten Treatmenteffekten in der logistischen Regression: Eine Shiny Application (Bachelor's Thesis). Friedrich-Schiller-University Jena, Germany."
output_help_general.d4 <- "https://gitlab.com/matthiasgehlert/bachelorthesis"
output_MEM.d <- "The marginal effects at the mean (MEM) are calculated based on the (\\(X,Z\\))-conditional probability \\(P(Y{=}1|X,Z)\\), where \\(Z{=}\\mu_{Z}\\). This is only an aggregation on the log odds scale. Again this principle of aggregation can be used to compute aggregated effects on the probability scale [e.g. \\(g_{1}(\\mu_{Z})\\)], the odds ratio scale and the log odds ratio scale. The conditional probability is almost never causally unbiased, due to aggregation bias. \\[g_{1}(\\mu_{Z}) = P(Y{=}1|X{=}1,Z{=}\\mu_{Z}) - P(Y{=}1|X{=}0,Z{=}\\mu_{Z})\\]\\[OR_{MEM} = \\frac{P(Y{=}1|X{=}1,Z{=}\\mu_{Z})}{1 - P(Y{=}1|X{=}1,Z{=}\\mu_{Z})} \\bigg/ \\frac{P(Y{=}1|X{=}0,Z{=}\\mu_{Z})}{1 - P(Y{=}1|X{=}0,Z{=}\\mu_{Z})}\\]\\[logOR_{MEM} = \\ln(OR_{MEM}) = E\\left[f_{1}(Z)\\right] = E\\left[\\lambda_{10} + \\lambda_{11}\\cdot Z\\right] = f_{1}(\\mu_{Z})\\]"
output_plots.d <- "Below you can select from different plots to visualize the conditional probabilities."
output_preview.d <- "The calculations are based on the models \\[Z \\sim \\mathcal{N}_{\\mu,\\sigma^{2}}\\]\\[P(X{=}1|Z) = \\frac{\\exp(\\kappa_{0} + \\kappa_{1} \\cdot Z)}{1 + \\exp(\\kappa_{0} + \\kappa_{1} \\cdot Z)}\\]\\[P(Y{=}1|X,Z) = \\frac{\\exp(\\lambda_{00} + \\lambda_{01} \\cdot Z + \\lambda_{10} \\cdot X + \\lambda_{11} \\cdot X \\cdot Z)}{1 + \\exp(\\lambda_{00} + \\lambda_{01} \\cdot Z + \\lambda_{10} \\cdot X + \\lambda_{11} \\cdot X \\cdot Z)}\\]"
output_SME.d <- "The simple marginal effects (SME) are calculated based on the conditional probability \\(P(Y{=}1|X)\\). In a strict sense, this is not an aggregation of the conditional probability, because \\(Z\\) is simply ignored. This principle can be used to compute aggregated effects on the probability scale (e.g. \\(\\beta_{1}\\)), the odds ratio scale and the log odds ratio scale. The conditional probability is causally unbiased, if the independent variables are stochastically independent (i.e., if \\(X \\perp \\!\\!\\! \\perp Z\\)). \\[\\beta_{1} = P(Y{=}1|X{=}1) - P(Y{=}1|X{=}0)\\]\\[OR_{SME} = \\frac{P(Y{=}1|X{=}1)}{1 - P(Y{=}1|X{=}1)} \\bigg/ \\frac{P(Y{=}1|X{=}0)}{1 - P(Y{=}1|X{=}0)}\\]\\[logOR_{SME} = \\ln(OR_{SME})\\]"
output_warning.d <- "<b>NOTE:</b> If you are satisfied with the input parameters, then confirm the input with either of the buttons and compute the aggregated effects and visualizations."
output_warning.d2 <- "<b>NOTE:</b> If you are satisfied with the input parameters, then increase the sample size (default: \\(N=0\\)) on the left. Else specify the input parameters in the model specification section first."

input_help.co1 <- paste0("input.", input_help.id, " == '", input_help.item1, "'")
input_help.co2 <- paste0("input.", input_help.id, " == '", input_help.item2, "'")
input_submit.co1 <- paste0("input.", input_submit.id, " == 0", " && ", "input.", input_submit.id2, " == 0", " && ", "input.", input_n.id, " == 0")
input_submit.co2 <- paste0("input.", input_submit.id, " > 0", " || ", "input.", input_submit.id2, " > 0", " || ", "input.", input_n.id, " != 0")
output_download.co1 <- paste0("input.", input_n.id, " == 0")
output_download.co2 <- paste0("input.", input_n.id, " != 0")
output_plots.co1 <- paste0("input.", output_plots.id, " == '", output_plots.item1, "'")
output_plots.co2 <- paste0("input.", output_plots.id, " == '", output_plots.item2, "'")
ui_tabs.co1 <- paste0("input.", ui_tabs.id, " == 1")
ui_tabs.co2 <- paste0("input.", ui_tabs.id, " == 2")
ui_tabs.co3 <- paste0("input.", ui_tabs.id, " == 3")

server <- function(input, output, session) {
	session$onSessionEnded(stopApp)
	observeEvent(
		eventExpr = {
			input[[input_example.id]]
		},
		{
			rnd_integer <- function(min, max, step) { as.numeric(sample(seq(min, max, step), 1)) }

			input_rnorm_mu.val8 <<- rnd_integer(input_rnorm_mu.min, input_rnorm_mu.max, input_rnorm_mu.step)
			input_rnorm_sd.val8 <<- rnd_integer(input_rnorm_sd.min, input_rnorm_sd.max, input_rnorm_sd.step)
			input_pX1gZ_kappa0.val8 <<- rnd_integer(input_pX1gZ_kappa0.min, input_pX1gZ_kappa0.max, input_pX1gZ_kappa0.step)
			input_pX1gZ_kappa1.val8 <<- rnd_integer(input_pX1gZ_kappa1.min, input_pX1gZ_kappa1.max, input_pX1gZ_kappa1.step)
			input_pY1gXZ_lambda00.val8 <<- rnd_integer(input_pY1gXZ_lambda00.min, input_pY1gXZ_lambda00.max, input_pY1gXZ_lambda00.step)
			input_pY1gXZ_lambda01.val8 <<- rnd_integer(input_pY1gXZ_lambda01.min, input_pY1gXZ_lambda01.max, input_pY1gXZ_lambda01.step)
			input_pY1gXZ_lambda10.val8 <<- rnd_integer(input_pY1gXZ_lambda10.min, input_pY1gXZ_lambda10.max, input_pY1gXZ_lambda10.step)
			input_pY1gXZ_lambda11.val8 <<- rnd_integer(input_pY1gXZ_lambda11.min, input_pY1gXZ_lambda11.max, input_pY1gXZ_lambda11.step)

			for (i in 1:8) {
				l0 <- eval(as.name(paste0("input_example.item", i)))
				l1 <- eval(as.name(paste0("input_rnorm_mu.val", i)))
				l2 <- eval(as.name(paste0("input_rnorm_sd.val", i)))
				l3 <- eval(as.name(paste0("input_pX1gZ_kappa0.val", i)))
				l4 <- eval(as.name(paste0("input_pX1gZ_kappa1.val", i)))
				l5 <- eval(as.name(paste0("input_pY1gXZ_lambda00.val", i)))
				l6 <- eval(as.name(paste0("input_pY1gXZ_lambda01.val", i)))
				l7 <- eval(as.name(paste0("input_pY1gXZ_lambda10.val", i)))
				l8 <- eval(as.name(paste0("input_pY1gXZ_lambda11.val", i)))

				if (input[[input_example.id]] == l0) {
					updateSliderInput(session, inputId = input_rnorm_mu.id, value = l1)
					updateSliderInput(session, inputId = input_rnorm_sd.id, value = l2)
					updateSliderInput(session, inputId = input_pX1gZ_kappa0.id, value = l3)
					updateSliderInput(session, inputId = input_pX1gZ_kappa1.id, value = l4)
					updateSliderInput(session, inputId = input_pY1gXZ_lambda00.id, value = l5)
					updateSliderInput(session, inputId = input_pY1gXZ_lambda01.id, value = l6)
					updateSliderInput(session, inputId = input_pY1gXZ_lambda10.id, value = l7)
					updateSliderInput(session, inputId = input_pY1gXZ_lambda11.id, value = l8)
				}
			}

			rm(rnd_integer, i)
			rm(l0, l1, l2, l3, l4, l5, l6, l7, l8)
		}
	)

	PREVIEW <- eventReactive(
		eventExpr = {
			if (input[[input_method.id]] == input_method.item2) {
				if (input[[input_submit.id]] == 0 & input[[input_submit.id2]] == 0 & input[[input_n.id]] == 0) {
					reactiveValuesToList(input)
				} else {
					if (input[[ui_tabs.id]] == 1) {
						values <- reactiveValues(
							a = input[[input_submit.id]],
							b = input[[input_submit.id2]]
						)
						reactiveValuesToList(values)
					} else if (input[[ui_tabs.id]] == 2) {
						values <- reactiveValues(
							a = input[[input_submit.id]],
							b = input[[input_submit.id2]],
							c = input[[input_n.id]]
						)
						reactiveValuesToList(values)
					}
				}
			} else {
				if (input[[ui_tabs.id]] == 1) {
					values <- reactiveValues(
						a = input[[input_rnorm_mu.id]],
						b = input[[input_rnorm_sd.id]],
						c = input[[input_pX1gZ_kappa0.id]],
						d = input[[input_pX1gZ_kappa1.id]],
						e = input[[input_pY1gXZ_lambda00.id]],
						f = input[[input_pY1gXZ_lambda01.id]],
						g = input[[input_pY1gXZ_lambda10.id]],
						h = input[[input_pY1gXZ_lambda11.id]],
						i = input[[input_submit.id]],
						j = input[[input_submit.id2]]
					)
					reactiveValuesToList(values)
				} else if (input[[ui_tabs.id]] == 2) {
					values <- reactiveValues(
						a = input[[input_n.id]]
					)
					reactiveValuesToList(values)
				}
			}
		},
		{
			is.example <- function(b = FALSE) {
				for (i in 1:8) {
					l0 <- eval(as.name(paste0("input_example.item", i)))
					l1 <- eval(as.name(paste0("input_rnorm_mu.val", i)))
					l2 <- eval(as.name(paste0("input_rnorm_sd.val", i)))
					l3 <- eval(as.name(paste0("input_pX1gZ_kappa0.val", i)))
					l4 <- eval(as.name(paste0("input_pX1gZ_kappa1.val", i)))
					l5 <- eval(as.name(paste0("input_pY1gXZ_lambda00.val", i)))
					l6 <- eval(as.name(paste0("input_pY1gXZ_lambda01.val", i)))
					l7 <- eval(as.name(paste0("input_pY1gXZ_lambda10.val", i)))
					l8 <- eval(as.name(paste0("input_pY1gXZ_lambda11.val", i)))
					if (
						input[[input_example.id]] == l0 &
						input[[input_rnorm_mu.id]] == l1 &
						input[[input_rnorm_sd.id]] == l2 &
						input[[input_pX1gZ_kappa0.id]] == l3 &
						input[[input_pX1gZ_kappa1.id]] == l4 &
						input[[input_pY1gXZ_lambda00.id]] == l5 &
						input[[input_pY1gXZ_lambda01.id]] == l6 &
						input[[input_pY1gXZ_lambda10.id]] == l7 &
						input[[input_pY1gXZ_lambda11.id]] == l8
					) {
						tmp <- paste0(l0)
						b <- TRUE
					}
				}
				if (b == FALSE) { tmp <- paste0("NONE, ", input_subsubtitle.lb2) }
				rm(i, b)
				rm(l0, l1, l2, l3, l4, l5, l6, l7, l8)
				tmp
			}

			tmp <- paste0(
				input_example.lb, " (currently selected): \n", is.example(), "\n\n",
				input_rnorm_mu.lb2, " = ", input[[input_rnorm_mu.id]], "\n",
				input_rnorm_sd.lb2, " = ", input[[input_rnorm_sd.id]], "\n",
				input_pX1gZ_kappa0.lb2, " = ", input[[input_pX1gZ_kappa0.id]], "\n",
				input_pX1gZ_kappa1.lb2, " = ", input[[input_pX1gZ_kappa1.id]], "\n",
				input_pY1gXZ_lambda00.lb2, " = ", input[[input_pY1gXZ_lambda00.id]], "\n",
				input_pY1gXZ_lambda01.lb2, " = ", input[[input_pY1gXZ_lambda01.id]], "\n",
				input_pY1gXZ_lambda10.lb2, " = ", input[[input_pY1gXZ_lambda10.id]], "\n",
				input_pY1gXZ_lambda11.lb2, " = ", input[[input_pY1gXZ_lambda11.id]]
			)

			rm(is.example)
			if (VERBOSE == TRUE) { cat("debug: PREVIEW run.\n") }
			tmp
		}
	)
	CALCULATIONS <- eventReactive(
		eventExpr = {
			PREVIEW()
		},
		{
			mu <- as.numeric(input[[input_rnorm_mu.id]])
			sd <- as.numeric(input[[input_rnorm_sd.id]])
			kappa0 <- as.numeric(input[[input_pX1gZ_kappa0.id]])
			kappa1 <- as.numeric(input[[input_pX1gZ_kappa1.id]])
			lambda00 <- as.numeric(input[[input_pY1gXZ_lambda00.id]])
			lambda01 <- as.numeric(input[[input_pY1gXZ_lambda01.id]])
			lambda10 <- as.numeric(input[[input_pY1gXZ_lambda10.id]])
			lambda11 <- as.numeric(input[[input_pY1gXZ_lambda11.id]])
			N <- as.numeric(input[[input_n.id]])

			EpY1gX0Z.lb <- "E[P(Y=1|X=0,Z)] = E[g0(Z)] ="
			EpY1gX1Z.lb <- "E[P(Y=1|X=1,Z)] ="
			pY1gX0Zmu.lb <- "P(Y=1|X=0,Z=mu_Z) = g0(mu_Z) ="
			pY1gX1Zmu.lb <- "P(Y=1|X=1,Z=mu_Z) ="
			pY1gX0.lb <- "P(Y=1|X=0) = beta0 ="
			pY1gX1.lb <- "P(Y=1|X=1) ="
			ATE_ADJ.lb <- "E[g1(Z)] ="
			ATE_MEM.lb <- "g1(mu_Z) ="
			ATE_SME.lb <- "beta1 ="
			OR_ADJ.lb <- "OR_ADJ ="
			OR_MEM.lb <- "OR_MEM ="
			OR_SME.lb <- "OR_SME ="
			logOR_ADJ.lb <- "logOR_ADJ ="
			logOR_MEM.lb <- "logOR_MEM = E[f1(Z)] ="
			logOR_SME.lb <- "logOR_SME ="
			biased.lb <- "*"
			biased.lb2 <- "\n--\n* = CAUSALLY BIASED"

			tmp <- list()

			if (input[[ui_tabs.id]] == 1) {
				if (input[[input_submit.id]] != 0 | input[[input_submit.id2]] != 0 | input[[input_n.id]] != 0) {
					EpY1gX0Z.calc <- integrate(f = function(Z) { plogis(lambda00 + lambda01 * Z) * dnorm(Z, mu, sd) }, lower = -Inf, upper = Inf)$value
					EpY1gX1Z.calc <- integrate(f = function(Z) { plogis(lambda00 + lambda01 * Z + lambda10 + lambda11 * Z) * dnorm(Z, mu, sd) }, lower = -Inf, upper = Inf)$value
					pY1gX0Zmu.calc <- plogis(lambda00 + lambda01 * mu)
					pY1gX1Zmu.calc <- plogis(lambda00 + lambda01 * mu + lambda10 + lambda11 * mu)
					pY1gX0.calc <- integrate(f = function(Z) { plogis(lambda00 + lambda01 * Z) * ((dnorm(Z, mu, sd) / (1 - integrate(f = function(Z) { plogis(kappa0 + kappa1 * Z) * dnorm(Z, mu, sd) }, lower = -Inf, upper = Inf)$value)) * plogis(-(kappa0 + kappa1 * Z))) }, lower = -Inf, upper = Inf)$value
					pY1gX1.calc <- integrate(f = function(Z) { plogis(lambda00 + lambda01 * Z + lambda10 + lambda11 * Z) * ((dnorm(Z, mu, sd) / integrate(f = function(Z) { plogis(kappa0 + kappa1 * Z) * dnorm(Z, mu, sd) }, lower = -Inf, upper = Inf)$value) * plogis(kappa0 + kappa1 * Z)) }, lower = -Inf, upper = Inf)$value

					ATE_ADJ.calc <- EpY1gX1Z.calc - EpY1gX0Z.calc
					ATE_MEM.calc <- pY1gX1Zmu.calc - pY1gX0Zmu.calc
					ATE_SME.calc <- pY1gX1.calc - pY1gX0.calc
					OR_ADJ.calc <- (EpY1gX1Z.calc / (1 - EpY1gX1Z.calc)) / (EpY1gX0Z.calc / (1 - EpY1gX0Z.calc))
					OR_MEM.calc <- (pY1gX1Zmu.calc / (1 - pY1gX1Zmu.calc)) / (pY1gX0Zmu.calc / (1 - pY1gX0Zmu.calc))
					OR_SME.calc <- (pY1gX1.calc / (1 - pY1gX1.calc)) / (pY1gX0.calc / (1 - pY1gX0.calc))
					logOR_ADJ.calc <- log(OR_ADJ.calc)
					logOR_MEM.calc <- log(OR_MEM.calc)
					logOR_SME.calc <- log(OR_SME.calc)

					if (!any(round(ATE_MEM.calc, 2) %in% c(round(ATE_ADJ.calc, 2) - 0.01, round(ATE_ADJ.calc, 2), round(ATE_ADJ.calc, 2) + 0.01))) { ATE_MEM_biased.lb <- biased.lb } else { ATE_MEM_biased.lb <- "" }
					if (!any(round(ATE_SME.calc, 2) %in% c(round(ATE_ADJ.calc, 2) - 0.01, round(ATE_ADJ.calc, 2), round(ATE_ADJ.calc, 2) + 0.01))) { ATE_SME_biased.lb <- biased.lb } else { ATE_SME_biased.lb <- "" }
					if (!any(round(OR_MEM.calc, 2) %in% c(round(OR_ADJ.calc, 2) - 0.01, round(OR_ADJ.calc, 2), round(OR_ADJ.calc, 2) + 0.01))) { OR_MEM_biased.lb <- biased.lb } else { OR_MEM_biased.lb <- "" }
					if (!any(round(OR_SME.calc, 2) %in% c(round(OR_ADJ.calc, 2) - 0.01, round(OR_ADJ.calc, 2), round(OR_ADJ.calc, 2) + 0.01))) { OR_SME_biased.lb <- biased.lb } else { OR_SME_biased.lb <- "" }
					if (!any(round(logOR_MEM.calc, 2) %in% c(round(logOR_ADJ.calc, 2) - 0.01, round(logOR_ADJ.calc, 2), round(logOR_ADJ.calc, 2) + 0.01))) { logOR_MEM_biased.lb <- biased.lb } else { logOR_MEM_biased.lb <- "" }
					if (!any(round(logOR_SME.calc, 2) %in% c(round(logOR_ADJ.calc, 2) - 0.01, round(logOR_ADJ.calc, 2), round(logOR_ADJ.calc, 2) + 0.01))) { logOR_SME_biased.lb <- biased.lb } else { logOR_SME_biased.lb <- "" }

					if ("*" %in% c(ATE_MEM_biased.lb, OR_MEM_biased.lb, logOR_MEM_biased.lb)) { any_MEM_biased.lb <- biased.lb2 }
					if ("*" %in% c(ATE_SME_biased.lb, OR_SME_biased.lb, logOR_SME_biased.lb)) { any_SME_biased.lb <- biased.lb2 }

					EpY1gX0Z.calc <- paste0(EpY1gX0Z.lb, " ", round(EpY1gX0Z.calc, 5))
					EpY1gX1Z.calc <- paste0(EpY1gX1Z.lb, " ", round(EpY1gX1Z.calc, 5))
					pY1gX0Zmu.calc <- paste0(pY1gX0Zmu.lb, " ", round(pY1gX0Zmu.calc, 5))
					pY1gX1Zmu.calc <- paste0(pY1gX1Zmu.lb, " ", round(pY1gX1Zmu.calc, 5))
					pY1gX0.calc <- paste0(pY1gX0.lb, " ", round(pY1gX0.calc, 5))
					pY1gX1.calc <- paste0(pY1gX1.lb, " ", round(pY1gX1.calc, 5))

					ATE_ADJ.calc <- paste0(ATE_ADJ.lb, " ", round(ATE_ADJ.calc, 5))
					ATE_MEM.calc <- paste0(ATE_MEM.lb, " ", round(ATE_MEM.calc, 5), ATE_MEM_biased.lb)
					ATE_SME.calc <- paste0(ATE_SME.lb, " ", round(ATE_SME.calc, 5), ATE_SME_biased.lb)
					OR_ADJ.calc <- paste0(OR_ADJ.lb, " ", round(OR_ADJ.calc, 5))
					OR_MEM.calc <- paste0(OR_MEM.lb, " ", round(OR_MEM.calc, 5), OR_MEM_biased.lb)
					OR_SME.calc <- paste0(OR_SME.lb, " ", round(OR_SME.calc, 5), OR_SME_biased.lb)
					logOR_ADJ.calc <- paste0(logOR_ADJ.lb, " ", round(logOR_ADJ.calc, 5))
					logOR_MEM.calc <- paste0(logOR_MEM.lb, " ", round(logOR_MEM.calc, 5), logOR_MEM_biased.lb)
					logOR_SME.calc <- paste0(logOR_SME.lb, " ", round(logOR_SME.calc, 5), logOR_SME_biased.lb)

					pY1gXZ.calc <- ggplot(data = data.frame(z = 0), mapping = aes(z = z))
					pY1gXZ.calc <- pY1gXZ.calc + stat_function(fun = function(z) { plogis(lambda00 + lambda01 * z) }, aes(color = calculations_plots_legend.lb), size = 1)
					pY1gXZ.calc <- pY1gXZ.calc + stat_function(fun = function(z) { plogis(lambda00 + lambda01 * z + lambda10 + lambda11 * z) }, aes(color = calculations_plots_legend.lb2), size = 1)
					pY1gXZ.calc <- pY1gXZ.calc + scale_color_manual("", values = c(COLORSCHEME[3], COLORSCHEME[4]))
					pY1gXZ.calc <- pY1gXZ.calc + xlim(-8, 8)
					pY1gXZ.calc <- pY1gXZ.calc + ylim(0, 1)
					pY1gXZ.calc <- pY1gXZ.calc + xlab(label = calculations_plots_axis.lb)
					pY1gXZ.calc <- pY1gXZ.calc + ylab(label = calculations_plots_axis.lb2)
					pY1gXZ.calc <- pY1gXZ.calc + theme_minimal()
					pY1gXZ.calc <- pY1gXZ.calc + theme(legend.title = element_blank(), axis.title.y = element_blank())
					pY1gXZ.calc <- pY1gXZ.calc + theme(text = element_text(size = 15, colour = COLORSCHEME[5]))

					pX1gZ.calc <- ggplot(data = data.frame(z = 0), mapping = aes(z = z))
					pX1gZ.calc <- pX1gZ.calc + stat_function(fun = function(z) { plogis(kappa0 + kappa1 * z) }, aes(color = calculations_plots2_legend.lb), size = 1)
					pX1gZ.calc <- pX1gZ.calc + scale_color_manual("", values = c(COLORSCHEME[2]))
					pX1gZ.calc <- pX1gZ.calc + xlim(-8, 8)
					pX1gZ.calc <- pX1gZ.calc + ylim(0, 1)
					pX1gZ.calc <- pX1gZ.calc + xlab(label = calculations_plots2_axis.lb)
					pX1gZ.calc <- pX1gZ.calc + ylab(label = calculations_plots2_axis.lb2)
					pX1gZ.calc <- pX1gZ.calc + theme_minimal()
					pX1gZ.calc <- pX1gZ.calc + theme(legend.title = element_blank(), axis.title.y = element_blank())
					pX1gZ.calc <- pX1gZ.calc + theme(text = element_text(size = 15, colour = COLORSCHEME[5]))

					tmp$output_ADJ.calc <- paste0(EpY1gX0Z.calc, "\n", EpY1gX1Z.calc)
					tmp$output_ADJ.calc2 <- paste0(ATE_ADJ.calc, "\n", OR_ADJ.calc, "\n", logOR_ADJ.calc)
					tmp$output_MEM.calc <- paste0(pY1gX0Zmu.calc, "\n", pY1gX1Zmu.calc)
					tmp$output_MEM.calc2 <- paste0(ATE_MEM.calc, "\n", OR_MEM.calc, "\n", logOR_MEM.calc, ifelse(exists("any_MEM_biased.lb"), any_MEM_biased.lb, ""))
					tmp$output_SME.calc <- paste0(pY1gX0.calc, "\n", pY1gX1.calc)
					tmp$output_SME.calc2 <- paste0(ATE_SME.calc, "\n", OR_SME.calc, "\n", logOR_SME.calc, ifelse(exists("any_SME_biased.lb"), any_SME_biased.lb, ""))

					tmp$output_plots_pX1gZ.calc <- pX1gZ.calc
					tmp$output_plots_pY1gXZ.calc <- pY1gXZ.calc
				}
			}

			if (input[[ui_tabs.id]] == 2) {
					tmp$output_download.calc <- NA

				if (N != 0) {
					Z.smpl <- rnorm(N, mu, sd)
					X.smpl <- rbinom(N, 1, plogis(kappa0 + kappa1 * Z.smpl))
					Y.smpl <- rbinom(N, 1, plogis(lambda00 + lambda01 * Z.smpl + lambda10 * X.smpl + lambda11 * X.smpl * Z.smpl))

					beta0.md <- suppressWarnings(lm(Y.smpl ~ X.smpl))$coefficients[[1]]
					beta1.md <- suppressWarnings(lm(Y.smpl ~ X.smpl))$coefficients[[2]]
					kappa0.md <- suppressWarnings(glm(X.smpl ~ Z.smpl, family = binomial))$coefficients[[1]]
					kappa1.md <- suppressWarnings(glm(X.smpl ~ Z.smpl, family = binomial))$coefficients[[2]]
					lambda00.md <- suppressWarnings(glm(Y.smpl ~ X.smpl * Z.smpl, family = binomial))$coefficients[[1]]
					lambda01.md <- suppressWarnings(glm(Y.smpl ~ X.smpl * Z.smpl, family = binomial))$coefficients[[3]]
					lambda10.md <- suppressWarnings(glm(Y.smpl ~ X.smpl * Z.smpl, family = binomial))$coefficients[[2]]
					lambda11.md <- suppressWarnings(glm(Y.smpl ~ X.smpl * Z.smpl, family = binomial))$coefficients[[4]]

					if (VERBOSE == TRUE) {
						cat("\n\tbeta0.md", beta0.md, "\n")
						cat("\tbeta1.md", beta1.md, "\n")
						cat("\tkappa0.md", kappa0.md, "\n")
						cat("\tkappa1.md", kappa1.md, "\n")
						cat("\tlambda00.md", lambda00.md, "\n")
						cat("\tlambda01.md", lambda01.md, "\n")
						cat("\tlambda10.md", lambda10.md, "\n")
						cat("\tlambda11.md", lambda11.md, "\n")
					}

					tmp$output_download.calc <- data.frame(ID = as.integer(seq(from = 1, to = N, by = 1)), Z = Z.smpl, X = X.smpl, Y = Y.smpl)
				}
			}

			rm(list = ls()[grep("*.lb|.calc|.smpl|.md", ls())])
			if (VERBOSE == TRUE) { cat("debug: CALCULATIONS run.\n") }
			tmp
		}
	)

	output[[output_download.id]] <- downloadHandler(
		contentType = "text/csv",
		filename = function() {
			paste0(ui_title.lb3, "_df_", rnd(), ".csv")
		},
		content = function(file) {
			tmp <- file.path(tempdir(), paste0("downloadHandler_id_", output_download.id, ".csv"))
			write.table(x = CALCULATIONS()$output_download.calc, file = tmp, row.names = FALSE, col.names = TRUE, sep = ",", na = "NA")
			file.rename(from = tmp, to = file)
		}
	)
	output[[output_download.id2]] <- renderTable(head(CALCULATIONS()$output_download.calc), bordered = TRUE, align = "c", width = "100%")
	output[[output_preview.id]] <- renderText(PREVIEW())
	output[[output_preview.id2]] <- renderText(PREVIEW())
	output[[output_ADJ.id]] <- renderText(CALCULATIONS()$output_ADJ.calc2)
	output[[output_ADJ.id2]] <- renderText(CALCULATIONS()$output_ADJ.calc)
	output[[output_MEM.id]] <- renderText(CALCULATIONS()$output_MEM.calc2)
	output[[output_MEM.id2]] <- renderText(CALCULATIONS()$output_MEM.calc)
	output[[output_SME.id]] <- renderText(CALCULATIONS()$output_SME.calc2)
	output[[output_SME.id2]] <- renderText(CALCULATIONS()$output_SME.calc)
	output[[output_plots_pX1gZ.id]] <- renderPlot(CALCULATIONS()$output_plots_pX1gZ.calc, bg = COLORSCHEME[1])
	output[[output_plots_pY1gXZ.id]] <- renderPlot(CALCULATIONS()$output_plots_pY1gXZ.calc, bg = COLORSCHEME[1])
}

ui <- fluidPage(
	title = ui_title.lb2,
	tags$head(tags$style(HTML(css))),
	withMathJax(),
	sidebarLayout(
		position = "left",
		fluid = TRUE,
		sidebarPanel(
			width = 4,
			genrow1(tags$div(class = "title", ui_title.lb)),
			genrow1(tags$div(class = "subtitle", ui_title.lb4)),
			genrow1(tags$hr()),
			conditionalPanel(
				condition = ui_tabs.co1,
				genrow1(tags$div(class = "subsubtitle", input_subsubtitle.lb)),
				genrow1(helpText(input_example.d)),
				genrow1(selectInput(inputId = input_example.id, label = input_example.lb, choices = list(input_example.item1, input_example.item2, input_example.item3, input_example.item4, input_example.item5, input_example.item6, input_example.item7, input_example.item8))),
				genrow1(tags$hr())
			),
			conditionalPanel(
				condition = ui_tabs.co2,
				genrow1(helpText(input_n.d)),
				genrow1(sliderInput(inputId = input_n.id, label = input_n.lb, min = input_n.min, max = input_n.max, value = input_n.min, step = input_n.step))
			),
			conditionalPanel(
				condition = ui_tabs.co1,
				genrow1(tags$div(class = "subsubtitle", input_subsubtitle.lb2)),
				genrow1(helpText(input_distribution.d)),
				genrow1(selectInput(inputId = input_distribution.id, label = input_distribution.lb, choices = list(input_distribution.item1))),
				genrow1(helpText(input_distribution.d2)),
				genrow2(tags$div(class = "adjustment-1", sliderInput(inputId = input_rnorm_mu.id, label = input_rnorm_mu.lb, min = input_rnorm_mu.min, max = input_rnorm_mu.max, value = input_rnorm_mu.val1, step = input_rnorm_mu.step)), tags$div(class = "adjustment-2", sliderInput(inputId = input_rnorm_sd.id, label = input_rnorm_sd.lb, min = input_rnorm_sd.min, max = input_rnorm_sd.max, value = input_rnorm_sd.val1, step = input_rnorm_sd.step))),
				genrow1(tags$hr()),
				genrow1(helpText(input_pX1gZ.d)),
				genrow2(tags$div(class = "adjustment-1", sliderInput(inputId = input_pX1gZ_kappa0.id, label = input_pX1gZ_kappa0.lb, min = input_pX1gZ_kappa0.min, max = input_pX1gZ_kappa0.max, value = input_pX1gZ_kappa0.val1, step = input_pX1gZ_kappa0.step)), tags$div(class = "adjustment-2", sliderInput(inputId = input_pX1gZ_kappa1.id, label = input_pX1gZ_kappa1.lb, min = input_pX1gZ_kappa1.min, max = input_pX1gZ_kappa1.max, value = input_pX1gZ_kappa1.val1, step = input_pX1gZ_kappa1.step))),
				genrow1(tags$hr()),
				genrow1(helpText(input_pY1gXZ.d)),
				genrow2(helpText(input_pY1gXZ.d2), helpText(input_pY1gXZ.d3)),
				genrow2(tags$div(class = "adjustment-1", sliderInput(inputId = input_pY1gXZ_lambda00.id, label = input_pY1gXZ_lambda00.lb, min = input_pY1gXZ_lambda00.min, max = input_pY1gXZ_lambda00.max, value = input_pY1gXZ_lambda00.val1, step = input_pY1gXZ_lambda00.step)), tags$div(class = "adjustment-2", sliderInput(inputId = input_pY1gXZ_lambda10.id, label = input_pY1gXZ_lambda10.lb, min = input_pY1gXZ_lambda10.min, max = input_pY1gXZ_lambda10.max, value = input_pY1gXZ_lambda10.val1, step = input_pY1gXZ_lambda10.step))),
				genrow2(tags$div(class = "adjustment-1", sliderInput(inputId = input_pY1gXZ_lambda01.id, label = input_pY1gXZ_lambda01.lb, min = input_pY1gXZ_lambda01.min, max = input_pY1gXZ_lambda01.max, value = input_pY1gXZ_lambda01.val1, step = input_pY1gXZ_lambda01.step)), tags$div(class = "adjustment-2", sliderInput(inputId = input_pY1gXZ_lambda11.id, label = input_pY1gXZ_lambda11.lb, min = input_pY1gXZ_lambda11.min, max = input_pY1gXZ_lambda11.max, value = input_pY1gXZ_lambda11.val1, step = input_pY1gXZ_lambda11.step))),
				genrow1(tags$hr()),
				genrow1(helpText(input_submit.d)),
				genrow1(actionButton(inputId = input_submit.id, label = HTML(input_submit.lb), width = "100%"))
			),
			conditionalPanel(
				condition = ui_tabs.co3,
				genrow1(helpText(input_help.d)),
				genrow1(radioButtons(inputId = input_help.id, label = input_help.lb, choices = list(input_help.item1, input_help.item2))),
				genrow1(tags$hr()),
				genrow1(helpText(input_method.d)),
				genrow1(radioButtons(inputId = input_method.id, label = input_method.lb, choices = list(input_method.item1, input_method.item2)))
			)
		),
		mainPanel(
			width = 8,
			tabsetPanel(
				id = ui_tabs.id,
				type = "pills",
				selected = 1,
				tabPanel(
					title = ui_tabs.lb,
					value = 1,
					genrow2(verbatimTextOutput(output_preview.id, placeholder = TRUE), tags$div(class = "adjustment-3", helpText(output_preview.d))),
					genrow1(tags$hr()),
					conditionalPanel(
						condition = input_submit.co1,
						genrow1(helpText(HTML(output_warning.d))),
						genrow1(actionButton(inputId = input_submit.id2, label = HTML(input_submit.lb), width = "100%"))
					),
					conditionalPanel(
						condition = input_submit.co2,
						genrow1(tags$div(class = "subsubtitle", output_subsubtitle.lb)),
						genrow1(helpText(output_plots.d)),
						genrow1(selectInput(inputId = output_plots.id, label = output_plots.lb, choices = list(output_plots.item1, output_plots.item2), width = "100%")),
						conditionalPanel(
							condition = output_plots.co1,
							genrow1(plotOutput(output_plots_pY1gXZ.id))
						),
						conditionalPanel(
							condition = output_plots.co2,
							genrow1(plotOutput(output_plots_pX1gZ.id))
						),
						genrow1(tags$hr()),
						genrow1(tags$div(class = "subsubtitle", output_subsubtitle.lb2)),
						genrow2(helpText(output_SME.d), helpText(output_MEM.d)),
						genrow2(verbatimTextOutput(output_SME.id2, placeholder = TRUE), verbatimTextOutput(output_MEM.id2, placeholder = TRUE)),
						genrow2(verbatimTextOutput(output_SME.id, placeholder = TRUE), verbatimTextOutput(output_MEM.id, placeholder = TRUE)),
						genrow1(tags$hr()),
						genrow1(tags$div(class = "adjustment-4", helpText(output_ADJ.d))),
						genrow1(verbatimTextOutput(output_ADJ.id2, placeholder = TRUE)),
						genrow1(verbatimTextOutput(output_ADJ.id, placeholder = TRUE)),
						genrow1(helpText(output_ADJ.d2))
					)
				),
				tabPanel(
					title = ui_tabs.lb2,
					value = 2,
					genrow2(verbatimTextOutput(output_preview.id2, placeholder = TRUE), tags$div(class = "adjustment-3", helpText(output_preview.d))),
					genrow1(tags$hr()),
					conditionalPanel(
						condition = output_download.co1,
						genrow1(helpText(HTML(output_warning.d2)))
					),
					conditionalPanel(
						condition = output_download.co2,
						genrow2(helpText(output_download.d2), helpText(HTML(output_download.d))),
						genrow2(tableOutput(output_download.id2), downloadButton(outputId = output_download.id, label = output_download.lb))
					)
				),
				tabPanel(
					title = ui_tabs.lb3,
					value = 3,
					conditionalPanel(
						condition = input_help.co1,
						genrow1(helpText(BUILD)),
						genrow1(tags$div(class = "adjustment-5", helpText(HTML(output_help_general.d)))),
						genrow1(tags$hr()),
						genrow1(helpText(output_help_general.d2)),
						genrow1(tags$div(class = "adjustment-2", helpText(output_help_general.d3))),
						genrow1(tags$div(class = "adjustment-2", helpText(output_help_general.d4)))
					),
					conditionalPanel(
						condition = input_help.co2,
						genrow1(helpText(output_help_citation2.d)),
						genrow1(helpText(output_help_citation2.d2)),
						genrow1(tags$hr()),
						genrow1(helpText(output_help_citation.d6)),
						genrow1(tags$div(class = "adjustment-2", helpText(output_help_citation.d4))),
						genrow1(tags$div(class = "adjustment-2", helpText(output_help_citation.d5))),
						genrow1(tags$hr()),
						genrow1(helpText(output_help_citation.d7)),
						genrow1(tags$div(class = "adjustment-2", helpText(output_help_citation.d))),
						genrow1(tags$div(class = "adjustment-2", helpText(output_help_citation.d2))),
						genrow1(tags$div(class = "adjustment-2", helpText(output_help_citation.d3)))
					)
				)
			)
		)
	)
)

shinyApp(ui = ui, server = server)
